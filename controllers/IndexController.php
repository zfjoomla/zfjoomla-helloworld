<?php
/**
 * JoomlaZend
 * Zend Framework for Joomla
 * Red Black Tree LLC
 *
 *
 * @author rbsolutions (rbsoultions.us@gmail.com)
 * @copyright (c) 2010 Red Black Tree LLC
 * @category JoomlZend
 * @package ComZend_HelloWorld
 * @link http://joomlazend.rbsolutions.us
 * @version $Id:$
 */
defined ('_VALID_MOS') or
    die('Direct Access to this location is not allowed');
/**
 * IndexController
 *
 * Default controller
 *
 *
 * @author rbsolutions (rbsoultions.us@gmail.com)
 * @copyright (c) 2010 Red Black Tree LLC
 * @category JoomlZend
 * @package ComZend_HelloWorld
 */
class Helloworld_IndexController extends Core_Controller_Action
{
    /**
     * indexAction
     *
     * Shows Hello World
     */
    public function indexAction()
    {
        $this->view->hello = $this->view->translate->_("Hello World");
    }
    /**
     * adminindexAction
     * 
     * shows Hello World
     */
    public function adminindexAction()
    {
        $this->view->hello = $this->view->translate->_("Admin Hello World");
    }
}